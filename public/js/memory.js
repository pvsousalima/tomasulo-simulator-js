(function() {
	'use strict';

	function Memory(size) {
		this.data = new Array(size);
		this.size = size;
		for (var i = 0; i < this.data.length; ++i) {
			this.data[i] = i;
		}
	}

	Memory.prototype.check_address = function(addr) {
		if (addr >= this.size || addr < 0) {
			throw new Error('[MEMORY] Invalid address "' + addr + '"');
		}
	};

	Memory.prototype.load = function(addr) {
		this.check_address(addr);
		return this.data[addr];
	};

	Memory.prototype.store = function(addr, value) {
		this.check_address(addr);
		this.data[addr] = value;
	};

	if (typeof module === 'object') {
		module.exports = Memory;
	} else {
		this.Memory = Memory;
	}

}).call(this);
