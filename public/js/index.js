
// variaveis do formulario, acesso com jquery
var addd = $('#addd').val()
var subd = $('#subd').val()
var multd = $('#multd').val()
var divd = $('#divd').val()
var ld = $('#ld').val()
var sd = $('#sd').val()
var add = $('#add').val()
var beq = $('#beq').val()
var bnez = $('#bnez').val()
var daddui = $('#daddui').val()

// funcao que le os dados do input
function fetchDataFromInput() {
    addd = $('#addd').val()
    subd = $('#subd').val()
    multd = $('#multd').val()
    divd = $('#divd').val()
    ld = $('#ld').val()
    sd = $('#sd').val()
    add = $('#add').val()
    beq = $('#beq').val()
    bnez = $('#bnez').val()
    daddui = $('#daddui').val()
}

function init(program) {

    var System = {};

    System.memory = new Memory(4096);

    System.registerFile = new RegisterFile(32, 'F');

    System.commonDataBus = new CommonDataBus();

    System.reservationStations = {
		INT_1: new ReservationStation('INT_1'),
		INT_2: new ReservationStation('INT_2'),
		INT_3: new ReservationStation('INT_3'),

        ADD_1: new ReservationStation('ADD_1'),
        ADD_2: new ReservationStation('ADD_2'),
        ADD_3: new ReservationStation('ADD_3'),

        MUL_1: new ReservationStation('MUL_1'),
        MUL_2: new ReservationStation('MUL_2'),

        LOAD_1: new Buffer('LOAD_1', System.memory),
        LOAD_2: new Buffer('LOAD_2', System.memory),
        LOAD_3: new Buffer('LOAD_3', System.memory),
        LOAD_4: new Buffer('LOAD_4', System.memory),
        LOAD_5: new Buffer('LOAD_5', System.memory),
        LOAD_6: new Buffer('LOAD_6', System.memory),

        STORE_1: new Buffer('STORE_1', System.memory),
        STORE_2: new Buffer('STORE_2', System.memory),
        STORE_3: new Buffer('STORE_3', System.memory)
    };

    System.instructionTypes = {
        'ADDD': new InstructionType('ADDD', addd ? addd : 2, 0,
                                    [InstructionType.PARAMETER_TYPE_REGISTER,
                                     InstructionType.PARAMETER_TYPE_REGISTER,
                                     InstructionType.PARAMETER_TYPE_REGISTER],
                                    function(p) { return p[1] + p[2]; },
                                    [System.reservationStations['ADD_1'],
                                     System.reservationStations['ADD_2'],
                                     System.reservationStations['ADD_3']]),

        'SUBD': new InstructionType('SUBD', subd ? subd : 2, 0,
                                    [InstructionType.PARAMETER_TYPE_REGISTER,
                                     InstructionType.PARAMETER_TYPE_REGISTER,
                                     InstructionType.PARAMETER_TYPE_REGISTER],
                                    function(p) { return p[1] - p[2]; },
                                    [System.reservationStations['ADD_1'],
                                     System.reservationStations['ADD_2'],
                                     System.reservationStations['ADD_3']]),

        'MULTD': new InstructionType('MULTD', multd ? multd : 10, 0,
                                    [InstructionType.PARAMETER_TYPE_REGISTER,
                                     InstructionType.PARAMETER_TYPE_REGISTER,
                                     InstructionType.PARAMETER_TYPE_REGISTER],
                                    function(p) { return p[1] * p[2]; },
                                    [System.reservationStations['MUL_1'],
                                     System.reservationStations['MUL_2']]),

        'DIVD': new InstructionType('DIVD', divd ? divd : 40, 0,
                                    [InstructionType.PARAMETER_TYPE_REGISTER,
                                     InstructionType.PARAMETER_TYPE_REGISTER,
                                     InstructionType.PARAMETER_TYPE_REGISTER],
                                    function(p) { return p[1] / p[2]; },
                                    [System.reservationStations['MUL_1'],
                                     System.reservationStations['MUL_2']]),

        'LD': new InstructionType('LD', ld ? ld : 2, 0,
                                    [InstructionType.PARAMETER_TYPE_REGISTER,
                                     InstructionType.PARAMETER_TYPE_ADDRESS],
                                    function(p) { return this.memory.load(p[1]) },
                                    [System.reservationStations['LOAD_1'],
                                     System.reservationStations['LOAD_2'],
                                     System.reservationStations['LOAD_3'],
                                     System.reservationStations['LOAD_4'],
                                     System.reservationStations['LOAD_5'],
                                     System.reservationStations['LOAD_6']
									 ]),

        'SD': new InstructionType('SD', sd ? sd : 2, 1,
                                  [InstructionType.PARAMETER_TYPE_REGISTER,
                                   InstructionType.PARAMETER_TYPE_ADDRESS],
                                  function(p) { this.memory.store(p[1], p[0]); return p[0]; },
                                  [System.reservationStations['STORE_1'],
                                   System.reservationStations['STORE_2'],
                                   System.reservationStations['STORE_3']]),

        'ADD': new InstructionType('ADD', add ? add : 1, 0,
        							[InstructionType.PARAMETER_TYPE_REGISTER,
                                     InstructionType.PARAMETER_TYPE_REGISTER,
                                     InstructionType.PARAMETER_TYPE_REGISTER],
                                    function(p) { return p[1] + p[2]; },
                                    [System.reservationStations['INT_1'],
                                     System.reservationStations['INT_2'],
                                     System.reservationStations['INT_3']]),

        'BEQ': new InstructionType('BEQ', beq ? beq : 1, 0,
        							[InstructionType.PARAMETER_TYPE_REGISTER,
                                     InstructionType.PARAMETER_TYPE_REGISTER],
                                    function(p) { return p[1] + p[2]; },
                                    [System.reservationStations['INT_1'],
                                     System.reservationStations['INT_2'],
                                     System.reservationStations['INT_3']]),

		'BNEZ': new InstructionType('BNEZ', bnez? bnez : 1, 0,
        							[InstructionType.PARAMETER_TYPE_REGISTER,
                                     InstructionType.PARAMETER_TYPE_REGISTER],
                                    function(p) { return p[1] + p[2]; },
                                    [System.reservationStations['INT_1'],
                                     System.reservationStations['INT_2'],
                                     System.reservationStations['INT_3']]),

		'DADDUI': new InstructionType('DADDUI', daddui ? daddui : 2, 0,
									[InstructionType.PARAMETER_TYPE_REGISTER,
                                     InstructionType.PARAMETER_TYPE_REGISTER],
                                    function(p) { return p[1] + p[2]; },
                                    [System.reservationStations['ADD_1'],
                                     System.reservationStations['ADD_2'],
                                     System.reservationStations['ADD_3']])

		};

    return new Main(program, System);
};

var interval;

function initGUI(main){
    $('.station:not(.title)').remove();
    $('.instruction:not(.title)').remove();
    $('.register:not(.title)').remove();

    var OUTPUTVAR = {
        'newreg' : function(name){
            return `<li class="register">
                        <span class="name">
                            ${name}
                        </span>
                        <span class="value" id="reg-${name}">
                        </span>
                    </li>`;
        },
        'newstation' : function(name){
            return `<li class="station" id="station-${name}">
                        <span class="name">${name}</span>
                        <span id="${name}" class="time-remaining">
                        </span>
                        <span class="instruction-number">
                        </span>
                        <span class="state">
                        </span>
                        <span class="p1">
                        </span>
                        <span class="p2">
                        </span>
                        <span class="p3">
                        </span>
                    </li>`;
        },
        'newinst' : function(linenum, detail){
            return `<li class="instruction" id="inst-${linenum}">
                        <span class="inst-linenum">
                            ${linenum}
                        </span>
                        <span class="instruction-detail">
                            ${detail}
                        </span>
                        <span class="issue-time"></span>
                        <span class="exec-time"></span>
                        <span class="writeback-time"></span>
                    </li>`;
        },
        'newmem' : function(addr, value){
            return `<div id="m${addr}" class="memory">
                    <span class="addr">${addr}</span>
                    <span class="value">${value}</span>
                    </div>`;
        }
    };


    var html = '';
    for (var i = 0; i < main.system.registerFile.count; i++){
        html += OUTPUTVAR . newreg(main.system.registerFile.prefix + i);
    }
    $('#float-registers').get(0).innerHTML += ((html));

    var html = '';
    for (rs in main.system.reservationStations){
        html += OUTPUTVAR . newstation(rs);
    }
    $('#reservation-stations').get(0).innerHTML += ((html));

    var html = '';
    for (var i = 0; i < main.instructions.length; i++){
        var ii = main.instructions[i];
        html += OUTPUTVAR . newinst(ii.id, ii.type.name + ' ' + ii.parameters.join(', '));
    }
    $('#instruction-show').get(0).innerHTML += ((html));

    var html = '';
    for (var i = 0; i < main.system.memory.size; i++){
        html += OUTPUTVAR . newmem(i, main.system.memory.data[i]);
    }
    document.getElementById('memory-show').innerHTML = html ;
    $('#memory-show').on('click', '.memory', function() {
        editMemory(this.id.substring(1));
    });

}

function update(main) {

    document.getElementById('global-clock').innerText = main.system.clock;

    for (var i = 0; i < 32; ++i) {
        var busy = main.system.commonDataBus.getBusy(InstructionType.PARAMETER_TYPE_REGISTER, 'F' + i);
        var value = main.system.registerFile.get('F' + i);
        document.getElementById('reg-F' + i).innerText = value.toFixed(5);
    }

    $('#total-inst').text(main.instructions.length);
    for (var i = 0; i < main.instructions.length; ++i) {
        var ii = main.instructions[i];
        var inst = document.getElementById('inst-' + ii.id);
        inst.querySelector('.issue-time').innerText = ii.issueTime > 0 ? ii.issueTime : '';
        inst.querySelector('.exec-time').innerText = ii.executeTime > 0 ? ii.executeTime : '';
        inst.querySelector('.writeback-time').innerText = ii.writeBackTime > 0 ? ii.writeBackTime : '';
    }

    for (var name in main.system.reservationStations) {
        var station = main.system.reservationStations[name]
        var guiItem = document.getElementById('station-' + name);
        var _state;
        switch (station.state) {
            case ReservationStation.STATE_IDLE:
                _state = 'NO';
                break;
            case ReservationStation.STATE_ISSUE:
                _state = 'YES';
                break;
            case ReservationStation.STATE_EXECUTE:
                _state = 'YES';
                break;
            case ReservationStation.STATE_WRITE_BACK:
                _state = 'YES';
        }

        var _remain_time = -1;

        var currentStation = undefined

        guiItem.querySelector('.state').innerText = _state;

        if (station.state !== ReservationStation.STATE_IDLE) {
            if (station.instruction){
                _remain_time = station.instruction.time;
            }
            var _curInst = '[' + station.instruction.id + ']';
            currentStation = station
            guiItem.querySelector('.instruction-number').innerText = _curInst;
            for (var i = 0; i < 3; i++){
                var td = guiItem.querySelector('.p' + (i + 1));
                if (i >= station.parameters.length){
                    td.innerHTML = ' - ';
                }
                else{
                    if (station.tags[i]){
                        td.innerHTML = station.tags[i].name;
                    }
                    else {
                        td.innerHTML = station.parameters[i];
                    }
                }
            }
        } else {
            guiItem.querySelector('.instruction-number').innerText = '';
            for (var i = 0; i < 3; i++){
                var td = guiItem.querySelector('.p' + (i + 1));
                    td.innerHTML = '';
            }
        }
        if(currentStation !== undefined) {
            guiItem.querySelector('#'+currentStation.name).innerText = _remain_time >= 0 ? _remain_time : ''
        }
    }

    $('#cur-pc').text(main.issuedInstructions);

    for (var i = 0; i < main.system.memory.size; i++){
        document.getElementById('m' + i).children[1].innerText = main.system.memory.data[i];
    }
}

$(function(){

    var program = `ld F6, 34 \nmultd f0,f2,f4 \nsubd f8, f6, f2 \ndivd f10, f0, f6 \naddd f6, f8, f2`

    var main = init(program);

    initGUI(main);

    update(main);

    $('#dialog').dialog({
         autoOpen: false,
         resizable: false,
         height: 500,
         width: 600,
         modal: true,
    });

    $('#dialog2').dialog({
         autoOpen: false,
         resizable: false,
         height: 100,
         width: 600,
         modal: true,
    });

    $('#action-step').button().on('click', function() {
        var done = main.step();
        update(main);
        if (done && interval) {
            clearInterval(interval);
            $('#action-run').attr('disabled', null);
        }
    });

    $('#action-run').button().on('click', function() {
        $('#action-run').attr('disabled', 'disabled');
        interval = setInterval(function(){$('#action-step').click();}, 100);
    });

    $('#action-stop').button().on('click', function() {
        clearInterval(interval);
        $('#action-run').attr('disabled', null);
        update(main);
    });

    $('#action-end').button().on('click', function() {
        main.run();
        update(main);
        $('#action-run').attr('disabled', null);
        if (interval) {
            clearInterval(interval);
        }
    });

    $('#action-restart').button().on('click', function(){
        if (interval) {
            clearInterval(interval);
        }
        main = init(program);
        initGUI(main);
        update(main);
    });

    $('#action-inst-edit').on('click', function(){
        $('#inst-edit-input').val(program);
        $('#dialog').dialog('open');
    });

    $('#inst-submit').button().click(function(){
        program = $('#inst-edit-input').val();
        try{
            $('#action-restart').click();
            $('#dialog').dialog('close');
        }
        catch(e){
            alert("Processo errado, verifique a repeticao");
        }
    });

    var bwExpand = true;
    $('.bottom-wrap .explain').on('click', function() {
        if (bwExpand) {
            bwExpand = false;
            $('.bottom-wrap').animate({
                height: 26
            }, 500);
            $('.top-wrap').animate({
                bottom: 26
            }, 500);
        } else {
            bwExpand = true;
            $('.bottom-wrap').animate({
                height: 300
            }, 500);
            $('.top-wrap').animate({
                bottom: 300
            }, 500);

        }
    });


    $('.modal').modal({
        dismissible: true, // Modal can be dismissed by clicking outside of the modal
        opacity: .5, // Opacity of modal background
        inDuration: 300, // Transition in duration
        outDuration: 200, // Transition out duration
        startingTop: '4%', // Starting top style attribute
        endingTop: '10%', // Ending top style attribute
        ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
        },
      }
    );


});
